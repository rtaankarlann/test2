import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Soft {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    softName: string;

    @Column()
    vendor: string;

    @Column({
        nullable: true
    })
    license: string;

}
