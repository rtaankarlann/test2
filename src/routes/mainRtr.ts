import { Router } from 'express';
import { index, about } from '../controllers/mainCtrl';

const mainRouter = Router();

mainRouter.get("/about", about);
mainRouter.get("/", index);

export { mainRouter };