import { Router } from 'express';
import { getUser, addUser } from '../controllers/userCtrl';

const userRouter = Router();

userRouter.post("/create", addUser);
userRouter.get("/", getUser);

export { userRouter };