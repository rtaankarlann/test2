import { Router } from 'express';
import { getSoft, addSoft } from '../controllers/SoftCtrl';

const softRouter = Router();

softRouter.post("/create", addSoft);
softRouter.get("/", getSoft);

export { softRouter };