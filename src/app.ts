import express from 'express';
import { userRouter } from './routes/userRtr';
import { mainRouter } from './routes/mainRtr';
import { softRouter } from './routes/softRtr';
import dotenv from "dotenv";
import { createConnection, ConnectionOptions } from "typeorm";

const options: ConnectionOptions = {
    type: 'sqlite',
    database: 'main.db',
    entities: [
       __dirname + "/entity/*.js"
   ],
   synchronize: true,
};

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

dotenv.config();

const port = process.env.SERVER_PORT;

app.use("/users", userRouter);
app.use("/", mainRouter);
app.use("/soft", softRouter);

app.use( (req, res, next) => {
    res.status(404).send("Not Found")
});

createConnection(options).then( connection => {
    app.listen( port, () => {
        // tslint:disable-next-line:no-console
        console.log( `server started at http://localhost:${ port }` );
    });
}).catch(error => console.log(error));