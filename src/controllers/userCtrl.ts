import { User } from ".././entity/user";
import { getConnection } from "typeorm";

export function addUser (request: any, response: any){
    if(!request.body) return response.sendStatus(400);
    console.log(request.body);
    response.send("Добавление пользователя");
};

export async function getUser (request: any, response: any){
    const repository = getConnection().getRepository(User);
    const allUsers = await repository.find();
    response.send(JSON.stringify(allUsers));
};