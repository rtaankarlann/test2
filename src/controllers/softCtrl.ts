import { Soft } from ".././entity/soft";
import { getConnection } from "typeorm";

export function addSoft (request: any, response: any){
    if(!request.body) return response.sendStatus(400);
    console.log(request.body);
    response.send("Добавление ПО");
};

export async function getSoft (request: any, response: any){
    const repository = getConnection().getRepository(Soft);
    const allSoft = await repository.find();
    response.send(JSON.stringify(allSoft));
};